#include "iotivity_config.h"
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_PTHREAD_H
#include <pthread.h>
#endif
#ifdef HAVE_WINDOWS_H
#include <Windows.h>
#endif
#include <string>
#include <map>
#include <cstdlib>
#include <mutex>
#include <condition_variable>
#include "OCPlatform.h"
#include "OCApi.h"

/*-------------JsonCPP-------------*/
#include <fstream>
#include <jsoncpp/json/json.h> // or jsoncpp/json.h , or json/json.h etc.
/*-------------JsonCPP-------------*/
#include <ctime>
#include "redoxlib.hpp"
#include "jsoncpplib.hpp"

std::map<std::string, int >RepPayloadMap;

#include "hiredisClientnew.c"

using namespace OC;
using namespace std;

#define  discoverying   1302
#define  getting        754
#define  posting        772
#define  observeing     1076

void  Auto_Discovery(int );

std::string UniqueID;
std::string space1 = " ";
JsonParsestruct JPP;
bool auto_Discovery=false;

struct attributetype
{
   int  i;
   double   d;
   bool     b;
   string str;
};

class t_resource
{
    public:
    std::string resourceURI;
    std::string serverID;
    std::string key;

    std::shared_ptr<OCResource> resource;
    OCRepPayloadValue* Attdetals;

    void onGet(const HeaderOptions& /*headerOptions*/, const OCRepresentation& rep, const int eCode);
    void getRepresentation( );
    void onPost(const HeaderOptions& /*headerOptions*/, const OCRepresentation& rep, const int eCode);
    void postRepresentation( OCRepresentation rep);
    void onObserve(const HeaderOptions /*headerOptions*/, const OCRepresentation& rep,
                        const int& eCode, const int& sequenceNumber);
    void observeRepresentation( );
};

std::map<string, t_resource*> t_resourcemap;
std::map<string, t_resource*> :: iterator t_resourcemapItr;       // t_resource map

std::map<string, t_resource*> t_resourceColmap;
std::map<string, t_resource*> :: iterator t_resourceColmapItr;       // t_resourceColmap map


class t_device
{
public:
    string Coluri;
    std::map<string, t_resource*> resMap;
};

class t_things
{
public:
string thingsName;
string thingsType;

string uuid;
string manufacturerName;
string osName;
string firmwareVersion;
string hardwareVersion;

std::vector<t_device*> deviceVec;
};

std::map<string, t_things*> t_thingsmap;                          // t_things map
static OCConnectivityType connectivityType = CT_ADAPTER_IP;

static ObserveType OBSERVE_TYPE_TO_USE = ObserveType::Observe;
REDIS hashredis;

using std::placeholders::_1;
using std::placeholders::_2;
using std::placeholders::_3;
using std::placeholders::_4;

int cont=0;         // testing


void t_resource::onGet(const HeaderOptions& /*headerOptions*/, const OCRepresentation& rep, const int eCode)
{
    cout << "In onGet callback" << endl;
    try
    {
        if(eCode == OC_STACK_OK){

            std::cout <<" Resource Uri ResourceUriHead_onGet getUri :"<< rep.getUri() << std::endl;
		// hashredis.Writeredisget( this->key, rep);
            this->Attdetals=rep.getPayload()->values;
            }
        else {
            std::cout << "onGET Response error: " << eCode << std::endl;
            }
    }
    catch(std::exception& e)
    {
        std::cout << "Exception: " << e.what() << " in onGet" << std::endl;
    }
}

void t_resource::getRepresentation( )
{
        std::cout << "Getting Representation..."<<std::endl;
        // Invoke resource's get API with the callback parameter

        QueryParamsMap test;
        this->resource->get(test, std::bind( &t_resource::onGet,this,_1,_2,_3));
}

void t_resource::onPost(const HeaderOptions& /*headerOptions*/, const OCRepresentation& rep, const int eCode)
{
       try
      {
          if (eCode == OC_STACK_OK || eCode == OC_STACK_RESOURCE_CHANGED)
          {
              	hashredis.Writeredisget( this->key, rep);
              std::cout << "onPOST Response done: " << eCode << std::endl;
          }
          else
          {
              std::cout << "onPOST Response error: " << eCode << std::endl;
              std::exit(-1);
          }
      }

      catch(std::exception& e)
      {
          std::cout << "Exception: " << e.what() << " in onPost" << std::endl;
      }
 }

void  t_resource::postRepresentation(OCRepresentation rep )
{
    std::cout << "posting Representation..."<<std::endl;
    // Invoke resource's get API with the callback parameter
    this->resource->post(rep, QueryParamsMap(), std::bind( &t_resource::onPost,this,_1,_2,_3) );

}

void t_resource::onObserve(const HeaderOptions /*headerOptions*/, const OCRepresentation& rep,
                    const int& eCode, const int& sequenceNumber)
{
    try
        {
            if(eCode == OC_STACK_OK && sequenceNumber <= MAX_SEQUENCE_NUMBER)
            {
                if(sequenceNumber == OC_OBSERVE_REGISTER)
                {
                    std::cout << "Observe registration action is successful" << std::endl;
                }


                std::cout << "Resource URI: "<<rep.getUri()<<std::endl;
                hashredis.Writeredisget( this->key, rep);
                std::cout << "Observe action happend" << std::endl;

                // representation(rep);
            }
            else
            {
                if(eCode == OC_STACK_OK)
                {
                    std::cout << "No observe option header is returned in the response." << std::endl;
                    std::cout << "For a registration request, it means the registration failed"
                            << std::endl;
                }
                else
                {
                    std::cout << "onObserve Response error: " << eCode << std::endl;
                    std::exit(-1);
                }
            }
        }
        catch(std::exception& e)
        {
            std::cout << "Exception: " << e.what() << " in onObserve" << std::endl;
        }


}

void t_resource::observeRepresentation( )
{
        std::cout << "observing Representation..."<<std::endl;
        // Invoke resource's get API with the callback parameter
        this->resource->observe(OBSERVE_TYPE_TO_USE, QueryParamsMap(),
                            std::bind( &t_resource::onObserve,this,_1,_2,_3,_4) );
}

void foundResource(std::shared_ptr<OCResource> resource)
{
//        std::cout << "In foundResource Callback start: "<< std::endl;
        t_resource *temp = new t_resource;

        temp->resource=resource;
        string hostAddress = resource->host();
        temp->resourceURI = resource->uri();
        temp->serverID = resource->sid();
        temp->key = temp->serverID + temp->resourceURI;

        if (!temp->resourceURI.compare("/oic/sec/doxm")==0
            &&!temp->resourceURI.compare("/oic/sec/pstat")==0
            &&!temp->resourceURI.compare("/oic/d")==0
            &&!temp->resourceURI.compare("/oic/p")==0
            &&!temp->resourceURI.compare("oic/d")==0
            &&!temp->resourceURI.compare("oic/p")==0 )
        {
                std::vector<std::string> interfaceVec;
                for(auto &resourceInterfaces : temp->resource->getResourceInterfaces())
                {
                        interfaceVec.push_back(resourceInterfaces);
                }


                if ( std::find(interfaceVec.begin(), interfaceVec.end(), "oic.if.ll" ) != interfaceVec.end())
                {

                        t_resourceColmap[temp->key] = temp;

                }
                else
                {
                        t_resourcemap[temp->key] = temp;
                }
                // std::cout << "Resource URI: " << temp->resourceURI << std::endl;
                // std::cout << "Server ID: " << temp->serverID << std::endl;
                // std::cout << "Host Address: " << hostAddress << std::endl;
        }
        alarm(1);
}


void discoveryFun()
{
    std::ostringstream requestURI;
OCStackResult r;                                       /////////////////         testing
	cout<<"In function Discovery"<<endl;
        // Find all resources
    requestURI << OC_RSRVD_WELL_KNOWN_URI;// << "?rt=oic/res";

	r=OCPlatform::findResource("", requestURI.str(),
                                    CT_DEFAULT, &foundResource);
	std::cout<< "Finding Resource after foundResource... "<< r <<std::endl;

}

void receivedDeviceInfo(const OCRepresentation& rep)
{
        t_things *temp = new t_things;
        rep.getValue("di", temp->uuid);
        rep.getValue("n",  temp->thingsName);
        rep.getValue("dt", temp->thingsType);

        std::string thingsName = " thingsName " + temp->thingsName;
        std::string thingsType = " thingsType " + temp->thingsType;

        // hashredis.Writeredisdiscovery( temp->uuid, thingsName );
        // hashredis.Writeredisdiscovery( temp->uuid, thingsType );

        t_thingsmap[temp->uuid] = temp;
}

void deviceInfo()
{
        cout << "In function deviceInfo " << endl;
    	std::string deviceDiscoveryURI   = "/oic/d";
        OCPlatform::getDeviceInfo("", deviceDiscoveryURI, connectivityType, &receivedDeviceInfo);
}

void receivedPlatformInfo(const OCRepresentation& rep)
{
//    std::cout << "\nPlatform Information received ---->\n";
        std::string PlatId;
        rep.getValue("pi", PlatId);

        auto it = t_thingsmap.find(PlatId);
        if (it != t_thingsmap.end( ))
        {
                rep.getValue("mnmn", it->second->manufacturerName);
                rep.getValue("mnos", it->second->osName);
                rep.getValue("mnhw", it->second->hardwareVersion);
                rep.getValue("mnfv", it->second->firmwareVersion);

                std::string manufacturerName = " manufacturerName " + it->second->manufacturerName;
                std::string osName = " osName " + it->second->osName;
                std::string hardwareVersion = " hardwareVersion " + it->second->hardwareVersion;
                std::string firmwareVersion = " firmwareVersion " + it->second->firmwareVersion;

                // hashredis.Writeredisdiscovery( PlatId, manufacturerName );
                // hashredis.Writeredisdiscovery( PlatId, osName );
                // hashredis.Writeredisdiscovery( PlatId, hardwareVersion );
                // hashredis.Writeredisdiscovery( PlatId, firmwareVersion );
        }

}

void platformInfo()
{
    cout << "In function platformInfo " << endl;
	std::string platformDiscoveryURI = "/oic/p";
	OCPlatform::getPlatformInfo("", platformDiscoveryURI, connectivityType, &receivedPlatformInfo);
}

void Discovery_all()
{
    cout << "In function Discovery_all "<<endl;
    	deviceInfo();
    	platformInfo();
        discoveryFun();
        std::cout << " after Discovery_all " << '\n';
}

void getResource(string key)
{
cout << "In function getResource" << endl;
QueryParamsMap test;
auto it = t_thingsmap.find(key);
if (it != t_thingsmap.end())
    {
//        it->second->getRepresentation( );
        // it->second->resource->get(test,&t_things::onGet);
    }
}

void postResource(string key,string attributeName,string inputValue)
{
    attributetype attributeValue;
    OCRepresentation rep;

    auto it = t_thingsmap.find(key);
    if (it != t_thingsmap.end())
        {
            auto it1 = RepPayloadMap.find(attributeName);
		    if (it1 != RepPayloadMap.end())
			cout<<"Attribute: "<<it1->second<<endl;

            //cout<<"enter attributeValue: "<<endl;
            switch(it1->second)
		    {
		case 0:
			cout<<"OCREP_PROP_NULL"<<endl;
			break;
		case 1:
			cout<<"OCREP_PROP_INT"<<endl;
            attributeValue.i=stoi(inputValue);
            rep.setValue(attributeName, attributeValue.i);
			break;
		case 2:
			cout<<"OCREP_PROP_DOUBLE"<<endl;
            attributeValue.d=stod(inputValue);
            rep.setValue(attributeName, attributeValue.d);
			break;
		case 3:
			cout<<"OCREP_PROP_BOOL"<<endl;
            if(inputValue == "true")
			attributeValue.b=1;
            else if(inputValue == "false")
            attributeValue.b=0;
            rep.setValue(attributeName, attributeValue.b);
			break;
		case 4:
			cout<<"OCREP_PROP_STRING"<<endl;
		    attributeValue.str=inputValue;
            rep.setValue(attributeName, attributeValue.str);
			break;
			}

//            it->second->postRepresentation(rep);
        }
}



void observeResource(string key)
{
cout << "In function getResource" << endl;
QueryParamsMap test;
auto it = t_thingsmap.find(key);
if (it != t_thingsmap.end())
    {
//        it->second->observeRepresentation( );

    }
}

int convertToASCII(string letter)
{

int m = 0;
    for (int i = 0; i < letter.length(); i++)
    {
        char x = letter.at(i);
        m+= int(x);
    }
return m;
}

void t_things_garbage()
{
        for(auto it = t_thingsmap.cbegin(); it != t_thingsmap.cend(); ++it)
        {
                if(!(it->second->manufacturerName.length()))
                {
                        t_thingsmap.erase(it->first);
                }
        }
}

void t_things_device()
{
        std::cout << " \nIN t_things_device : " << '\n';

        for(auto it_uid = t_thingsmap.cbegin(); it_uid != t_thingsmap.cend(); ++it_uid)
        {
            int i=0;
                for(auto it_key = t_resourceColmap.cbegin(); it_key != t_resourceColmap.cend(); ++it_key)
                {
                        if(!(it_key->first.find(it_uid->first) ) )
                        {
                            t_device *device= new t_device;

                                it_uid->second->deviceVec.push_back(device);
                                it_uid->second->deviceVec[i]->Coluri=it_key->first;
                                std::cout << it_uid->first <<"\t"<< it_key->first << '\n';
                                for(auto it_key = t_resourcemap.cbegin(); it_key != t_resourcemap.cend(); ++it_key)
                                {
                                        if(!(it_key->first.find(it_uid->second->deviceVec[i]->Coluri)))
                                        {
                                                it_uid->second->deviceVec[i]->resMap.insert(pair<string, t_resource*>(it_key->first,it_key->second));
                                        }
                                }
                            i++;
                        }
                }
        }
}

void t_resource_attPrint(OCRepPayloadValue* repp)
{
        if(repp == NULL)
        {
                std::cout << "/* message */" << '\n';
        }
        while(repp)
        {

                int datatype = repp->type;
                char* names = repp->name;
                std::string name(names);

                RepPayloadMap.insert ( std::pair<std::string, int > ( name, datatype ) );

                cout << "DataType :" << datatype <<endl;
                switch (datatype)
                {
                        cout << "\n\n\tSWITCH\n " <<endl;
                case 0:
                        cout << "OCREP_PROP_NULL " <<endl;
                        break;

                case 1:
                {
                        cout << " :: OCREP_PROP_INT :: " <<endl;
                        cout << "AttrName :" << name <<endl;
                        cout << "AttrValue:" << repp->i <<endl;
                }
                break;

                case 2:
                {
                        cout << "OCREP_PROP_DOUBLE  " <<endl;
                        cout << "AttrName :" << name <<endl;
                        cout << "AttrValue:" << repp->d <<endl;
                }
                break;

                case 3:
                {
                        cout << "OCREP_PROP_BOOL  " <<endl;
                        cout << "AttrName :" << name <<endl;
                        cout << "AttrValue:" << boolalpha << repp->b <<endl;
                }
                break;

                case 4:
                {
                        cout << "OCREP_PROP_STRING  " <<endl;
                        cout << "AttrName :" << name <<endl;
                        cout << "AttrValue:" << repp->str <<endl;
                }
                break;

                case 5:
                        cout<<"OCREP_PROP_BYTE_STRING"<<endl;
                        break;
                case 6:
                        cout<<"OCREP_PROP_OBJECT"<<endl;
                        break;
                case 7:
                        cout<<"OCREP_PROP_ARRAY"<<endl;
                        break;

                }

                repp = repp->next;
        }
        std::cout <<  '\n';
}

void t_things_print()
{
        std::cout << " \n t_things Map: " << '\n';
        for(auto it = t_thingsmap.cbegin(); it != t_thingsmap.cend(); ++it)
        {
                std::cout << it->first << " " << it->second->uuid  << " ";
                std::cout << it->second->thingsName << " " << it->second->thingsType  << "\n";
                std::cout << it->second->manufacturerName << " " << it->second->osName  << " ";
                std::cout << it->second->hardwareVersion << " " << it->second->firmwareVersion  << "\n";
                for (int i = 0; i < it->second->deviceVec.size(); i++)
                {
                        cout << it->second->deviceVec[i]->Coluri << "\n";
                    for(auto it_non = it->second->deviceVec[i]->resMap.cbegin(); it_non != it->second->deviceVec[i]->resMap.cend(); ++it_non)
                    {
                        std::cout << it_non->first <<"\t\t";
                        std::cout << it_non->second->resource << '\n';
                        t_resource_attPrint(it_non->second->Attdetals);
                    }
                }
                cout <<"\n"<< endl;
        }
}

void t_resource_att_details()
{
        QueryParamsMap test;
        for(auto it_uid = t_thingsmap.cbegin(); it_uid != t_thingsmap.cend(); ++it_uid)
        {
            for (int i = 0; i < it_uid->second->deviceVec.size(); i++)
            {
                for(auto it_non = it_uid->second->deviceVec[i]->resMap.cbegin(); it_non != it_uid->second->deviceVec[i]->resMap.cend(); ++it_non)
                {
                        std::cout << "get req sent :" <<
                        it_non->second->resource->get(test, std::bind( &t_resource::onGet,it_non->second,_1,_2,_3)) <<endl;
                        usleep(500000);
                }
            }
        }
}

void end_Discovery(int sig)
{
        std::cout << " \n in end_Discovery map: " << '\n';
        std::cout << " \n Non_Collection Map: " << '\n';
        for(auto it = t_resourcemap.cbegin(); it != t_resourcemap.cend(); ++it)
        {
                std::cout << it->first << "\t:\t" << it->second  << "\n";
        }
        std::cout << " \n Collection Map: " << '\n';
        for(auto it = t_resourceColmap.cbegin(); it != t_resourceColmap.cend(); ++it)
        {
                std::cout << it->first << "\t:\t" << it->second  << "\n";
        }

        t_things_garbage();
        t_things_device();
        t_resource_att_details();
        // sleep(1);
        t_things_print();

        if (auto_Discovery == true)
        {
                signal(SIGALRM, Auto_Discovery );
                alarm(3600);
        }
        else
        {
                signal(SIGALRM, SIG_IGN );
        }
}


void one_time_Discovery()
{
        signal(SIGALRM, end_Discovery );
        Discovery_all();
}

void  Auto_Discovery(int sig)
{
        printf("IN Auto_Discovery\n");
        one_time_Discovery();
//      alarm(3600);
}

void *myThread_Dis(void *p)
{
        int opt=*(int *)p;

        printf("IN myThread_Dis\n");

        switch(opt)
        {
        case 1: std::cout << "in one_time_Discovery" << '\n';
                one_time_Discovery();
                break;

        case 2: signal(SIGALRM, Auto_Discovery );
                auto_Discovery=true;
                std::cout << "in Auto_Discovery" << '\n';
                alarm(1);
                break;

        case 3: signal(SIGALRM, SIG_IGN );
                auto_Discovery=false;
                std::cout << "in Disable_Auto_Discovery" << '\n';
                break;

        default: std::cout << "in one_time_Discovery" << '\n';
                one_time_Discovery();
                break;
        }


        printf("OUT myThread_Dis\n");

}

void *ResControl(void *p)
{
int option=*(int *)p;
string attributeName,attributeValue;
string key;

    key=JPP.UUID+JPP.DevID;
    attributeName=JPP.attributeName1;
    attributeValue=JPP.attributeValue1;

        switch(option)
        	{
        	case getting:
                    getResource(key);
        		    sleep(2);
        		    break;

            case posting:
                    postResource(key,attributeName,attributeValue);
            		sleep(2);
            		break;

            case observeing:
                    observeResource(key);
            		sleep(2);
            		break;


        	default:
        		std::cout<<"Please enter a proper option "<<std::endl;

        	}
}

int main()
{
    std::cout.setf(std::ios::boolalpha);
    pthread_t dtid,ctid;
    int init = 1;

 myThread_Dis((void *)&init);

sleep(3);

// std::cout << " \n map: " << '\n';
// for(auto it = t_thingsmap.cbegin(); it != t_thingsmap.cend(); ++it)
// {
//     std::cout << it->first << " " << it->second  << "\n";
// }

while(1)
{
    std::string message = redox_sub();
    JPP = jsonReader(message);
    int request = convertToASCII(JPP.command);

        if(request==0)
        continue;

        switch(request)
        {
        case discoverying:
            {
                std::cout << "in discoverying" << '\n';
                pthread_create(&dtid,0,myThread_Dis,(void *)&init);
//                pthread_cancel(dtid);
            }
            break;
        default:
            {
                pthread_create(&ctid,0,ResControl,(void *)&request);
            }
            break;
        }

        }
    return 0;
}
