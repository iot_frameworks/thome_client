#include <stdlib.h>
#include <iostream>
#include "redox.hpp"

using namespace std;

std::string sub_topic = "shifu";
std::string pub_topic = "shifu1";
std::string message;

std::string redox_sub()
{
  redox::Subscriber subscriber;
  if(!subscriber.connect());

  auto got_message = [](const string& sub_topic, const string& msg) {

  cout << sub_topic << " : " << msg << endl;
  message = msg;
  };

  auto subscribed = [](const string& topic) {

  };

  message.clear();
  subscriber.subscribe(sub_topic, got_message, subscribed);
  this_thread::sleep_for(chrono::milliseconds(100));

return message;

}
